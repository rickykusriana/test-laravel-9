<?php

use App\Http\Controllers\CartController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ShopController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('{url}', function () {
    return view('app');
})->where('url', '(login|register|cart|shop|profile)');

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('getProducts', [HomeController::class, 'getProducts']);
Route::get('getCategories', [HomeController::class, 'getCategories']);

Route::get('getShop/{slug?}', [ShopController::class, 'getShop']);
Route::get('category/{slug}', [ShopController::class, 'category'])->name('category');

Route::get('countCart', [CartController::class, 'countCart']);
Route::get('getCart', [CartController::class, 'getCart']);
Route::post('addCart', [CartController::class, 'addCart'])->name('add.cart');
Route::post('updateCart', [CartController::class, 'updateCart'])->name('update.cart');
Route::get('emptyCart', [CartController::class, 'emptyCart'])->name('empty.cart');
Route::get('removeCart/{id}', [CartController::class, 'removeCart'])->name('remove.cart');

Route::get('checkout', [CheckoutController::class, 'index'])->name('checkout.index');
Route::post('checkout', [CheckoutController::class, 'checkout'])->name('checkout');
Route::get('getProvince', [CheckoutController::class, 'getProvince'])->name('get.province');
Route::get('getCity/{id}', [CheckoutController::class, 'getCity'])->name('get.city');
Route::post('getCost', [CheckoutController::class, 'getCost'])->name('get.cost');
