const helper = {
    install(app) {
        app.config.globalProperties.rupiah = (param) => {
            return "Rp. " + param.toString().replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1\.");
        }
    }
}
export default helper;