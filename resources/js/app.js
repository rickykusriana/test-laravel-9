// import './bootstrap';
import { createApp } from "vue";
import App from "./App.vue";
import Router from './router'
import Store from "./store";
import Helper from './helpers'
import Toaster from '@meforma/vue-toaster';
import Modal from './components/Modal.vue';

const app = createApp(App);
app.use(Router);
app.use(Store);
app.use(Helper);
app.use(Toaster, {
    position: 'top-right'
});
app.component("Modal", Modal);
app.mount("#app");