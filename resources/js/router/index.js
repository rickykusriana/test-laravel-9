import { createRouter, createWebHistory } from "vue-router";

import Home from "@/pages/Home.vue";
import Login from "@/pages/Login.vue";
import Register from "@/pages/Register.vue";
import Cart from "@/pages/Cart.vue";
import Shop from "@/pages/Shop.vue";
import Profile from "@/pages/Profile.vue";

const routes = [
    {
        path: "/",
        name: "home",
        component: Home
    },
    {
        path: "/login",
        name: "login",
        component: Login
    },
    {
        path: "/register",
        name: "register",
        component: Register
    },
    {
        path: "/cart",
        name: "cart",
        component: Cart
    },
    {
        path: "/shop",
        name: "shop",
        component: Shop
    },
    {
        path: "/category/:slug",
        name: "category",
        component: Shop,
        props: true
    },
    {
        path: "/profile",
        name: "profile",
        component: Profile
    },
];

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes,
});

// router.beforeEach((to, from, next) => {
//     const publicPages = ['/login', '/register', '/home'];
//     const authRequired = !publicPages.includes(to.path);
//     const loggedIn = localStorage.getItem('user');

//     // trying to access a restricted page + not logged in
//     // redirect to login page
//     if (authRequired && !loggedIn) {
//     next('/login');
//     } else {
//     next();
//     }
// });

export default router;
