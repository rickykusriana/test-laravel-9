import axios from "axios";

const API_URL = "http://localhost:8000/api/";

class AuthService {
    login(user) {
        return axios
            .post(API_URL + "login", {
                email: user.email,
                password: user.password,
            })
            .then((response) => {
                if (response.data.status) {
                    localStorage.setItem("user", JSON.stringify(response.data));
                }
                return response.data;
            });
    }

    logout() {
        localStorage.removeItem("user");
    }

    register(user) {
        return axios
            .post(API_URL + "register", {
                name: user.name,
                email: user.email,
                password: user.password,
                password_confirmation: user.password_confirmation
            })
            .then((response) => {
                if (response.data.status) {
                    localStorage.setItem("user", JSON.stringify(response.data));
                }
                return response.data;
            });
    }
}

export default new AuthService();
