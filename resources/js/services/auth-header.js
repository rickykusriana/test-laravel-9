export default function authHeader()
{
    let user = JSON.parse(localStorage.getItem("user"));

    if (user && user.authorization.token) {
        return { Authorization: "Bearer " + user.authorization.token };
        // return { 'x-access-token': user.accessToken };
    } else {
        return {};
    }
}
