<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function countCart()
    {
        $cart = session('cart') ? count(session('cart')) : 0;
        return response()->json($cart);
    }

    public function getCart()
    {
        $cart = session('cart');
        return response()->json($cart);
    }

    public function addCart(Request $request)
    {
        $product = Product::findOrFail($request->id);
        $cart = session()->get('cart', []);

        if (isset($cart[$request->id])) {
            $cart[$request->id]['quantity'] = $cart[$request->id]['quantity'] + $request->quantity;
        } else {
            $cart[$request->id] = [
                'id' => $request->id,
                "name" => $product->product_name,
                "quantity" => $request->quantity,
                "price" => $product->product_price,
                "image" => $product->product_image
            ];
        }

        session()->put('cart', $cart);
        session()->flash('success', 'Succesfully added to cart');
        return redirect()->back();
    }

    public function updateCart(Request $request)
    {
        $cart = session()->get('cart', []);
        foreach ($request->id as $key => $id) {
            $cart[$id]['quantity'] = $request->quantity[$key];
        }
        session()->put('cart', $cart);
        session()->flash('success', 'Cart updated');
        return redirect()->back();
    }

    public function removeCart($id)
    {
        if ($id) {
            $cart = session()->get('cart');
            if (isset($cart[$id])) {
                unset($cart[$id]);
                session()->put('cart', $cart);
            }
            session()->flash('success', 'Succesfully removed');
            return redirect()->back();
        }
    }

    public function emptyCart()
    {
        session()->forget('cart');
        session()->flash('success', 'Succesfully cleared');
        return redirect()->back();
    }
}
