<?php

namespace App\Http\Controllers;

use App\Models\Sales;
use App\Models\SalesDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Kodepandai\LaravelRajaOngkir\Facades\RajaOngkir;

class CheckoutController extends Controller
{
    function index()
    {
        return redirect('/');
    }

    public function checkout(Request $request)
    {
        $sales = Sales::create([
            'user_id' => $request->user_id,
            'order_id' => uniqid(),
            'order_total' => $request->order_total,
            'name' => $request->name,
            'email' => $request->email
        ]);

        $cart = session('cart');
        foreach ($cart as $id => $value) {
            $salesDetail = [
                'order_id' => $sales->id,
                'product_id' => $id,
                'price' => $value['price'],
                'quantity' => $value['quantity'],
                'subtotal' => $value['price'] * $value['quantity'],
                'total' => $request->order_total
            ];
        }

        SalesDetail::create($salesDetail);

        session()->forget('cart');
        session()->flash('success', 'Order Placed');
        return redirect('/profile');
    }

    public function getProvince()
    {
        return RajaOngkir::getProvince();
    }

    public function getCity($provinceId)
    {
        return RajaOngkir::getCity(null, $provinceId);
    }

    public function getSubdistrict($cityId)
    {
        return RajaOngkir::getSubdistrict($cityId);
    }

    public function getCost(Request $request)
    {
        return RajaOngkir::getCost(
            152,
            'city',
            (int)$request->destination,
            'city',
            1000,
            $request->courier
        );
    }
}
