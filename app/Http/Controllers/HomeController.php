<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;

class HomeController extends Controller
{
    function index()
    {
        return view('app');
    }

    function getProducts()
    {
        $products = Product::where('is_active', 1)
            ->orderBy('created_at', 'desc')->take(12)
            ->with('category')
            ->get();
        return response()->json($products);
    }

    function getCategories()
    {
        $categories = Category::where('is_active', 1)->get();
        return response()->json($categories);
    }
}
