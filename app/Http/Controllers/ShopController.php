<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    public $request;

    function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function category()
    {
        return view('app');
    }

    public function getShop($slug = null)
    {
        $search = $this->request->query('search');
        $products = Product::where('is_active', 1)
            ->where('product_name', 'LIKE', '%' . $search . '%')
            ->with('category')
            ->whereHas('category', function($query) use($search, $slug) {
                $query->where('category_name', 'LIKE', '%' . $search . '%');
                if ($slug) {
                    $query->where('slug', $slug);
                }
            })
            ->orderBy('created_at', 'desc');

        $data = [
            'products' => $products->paginate(9),
            'newProducts' => Product::with('category')->orderBy('created_at', 'desc')->take(3)->get()
        ];

        return response()->json($data);
    }
}
