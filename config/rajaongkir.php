<?php
// config for Kodepandai/LaravelRajaOngkir
return  [
    /**
     * api key yang di dapat dari akun raja ongkir
     */
    'API_KEY' => env('RAJAONGKIR_KEY', 'c0f86f3a66c68de9b19eb01f9e346f34'),

    /**
     * tipe akun untuk menentukan api url
     * starter, basic, pro
     */
    'ACCOUNT_TYPE' => env('RAJAONGKIR_TYPE', 'starter')
];
