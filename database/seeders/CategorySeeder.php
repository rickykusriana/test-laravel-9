<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'category_name' => 'T-Shirts',
                'category_desc' => fake()->text(),
                'slug' => 't-shirts',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'category_name' => 'Sweater & Hoodie',
                'category_desc' => fake()->text(),
                'slug' => 'sweater-hoodie',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'category_name' => 'Jeans',
                'category_desc' => fake()->text(),
                'slug' => 'jeans',
                'created_at' => now(),
                'updated_at' => now()
            ],
        ];
        Category::insert($data);
    }
}
