<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Load images
        $images = [];
        for ($i=1; $i < 12; $i++) {
            array_push($images, 'assets/imgs/shop/product-'.$i.'-1.jpg');
        }

        for ($i=0; $i < 50; $i++) {
            $name = fake()->name();

            $data = [
                'category_id' => rand(1,3),
                'product_name' => $name,
                'product_desc' => fake()->text(),
                'product_price' => fake()->randomNumber(6, true),
                'product_image' => $images[array_rand($images)],
                'slug' => Str::slug($name),
                'created_at' => now(),
                'updated_at' => now() ];
            Product::insert($data);
        }

    }
}
